import os
import csv
import pandas as pd
import matchzoo as mz

from configs import config


def load_data(path='train.csv', task='ranking'):
    """
    Load data.
    :param stage: One of `train`, `dev`, and `test`.
    :param task: Could be one of `ranking`, `classification` or a
        :class:`matchzoo.engine.BaseTask` instance.
    :return: A DataPack if `ranking`, a tuple of (DataPack, classes) if
        `classification`.
    """

    file_path = path
    data_pack = _read_data(file_path)

    if task == 'ranking':
        task = mz.tasks.Ranking()
    if task == 'classification':
        task = mz.tasks.Classification()

    if isinstance(task, mz.tasks.Ranking):
        return data_pack
    elif isinstance(task, mz.tasks.Classification):
        data_pack.one_hot_encode_label(task.num_classes, inplace=True)
        return data_pack, [False, True]
    else:
        raise ValueError(f"{task} is not a valid task.")


def _read_data(path):
    table = pd.read_csv(path, sep='\t', header=0, quoting=csv.QUOTE_NONE)
    df = pd.DataFrame({
        'text_left': table['q_text'],
        'text_right': table['d_text'],
        'id_left': table['qid'],
        'id_right': table['docid'],
        'label': table['label']
    })
    return mz.pack(df)


def save_result(model, args, data_pack_processed, name='train', index=0):
    """save result to csv"""
    x, _ = data_pack_processed.unpack()
    y_pred = model.predict(x)
    y_pred = [i[0] for i in y_pred]

    df = pd.DataFrame({'id_left': x['id_left'], 'id_right': x['id_right'],
                    'score': y_pred})
    df.sort_values(by=['id_left', 'score'], ascending=[
                   True, False], inplace=True)
    df['Q0'] = 'Q0'
    df['WAW'] = 'WAW'
    df['blank'] = '0'
    df['rank'] = df.groupby('id_left')['score'].rank(
        ascending=False).astype(int)

    pred = df[['id_left', 'Q0', 'id_right', 'rank', 'score', 'WAW']]
    pred.to_csv(
        f"./output/drmm_w2v_d{args.num_dup}n{args.num_neg}_top{args.top_k}_eps{args.epochs}_bs{args.batch_size}_{name}_{index}.txt", sep='\t', 
        index=False, header=False)



def train(args):
    """train model using cross validation"""

    train_path = os.path.join(args.dataset_path, 'train.csv')
    data_pack = load_data(path=train_path, task='ranking')

    # Build preprocessor
    preprocessor = mz.preprocessors.BasicPreprocessor(
            remove_stop_words=True)
    preprocessor.fit_transform(data_pack)
    # preprocessor.save(args.prepro_model)
    print('Preprocessor saved.')

    # Build model
    ranking_task = mz.tasks.Ranking(loss=mz.losses.RankHingeLoss())
    ranking_task.metrics = [
        mz.metrics.Precision(k=5),
        mz.metrics.Precision(k=10),
        mz.metrics.Precision(k=15)
    ]
    
    glove_embedding = mz.embedding.load_from_file(
        file_path=args.embed_path, mode='glove')
    embedding_matrix = glove_embedding.build_matrix(
        preprocessor.context['vocab_unit'].state['term_index'])

    # Cross validation
    for index in range(args.cv_num):

        model = mz.models.DRMMTKSModel()
        model.params['input_shapes'] = preprocessor.context['input_shapes']
        model.params['task'] = ranking_task
        model.params['top_k'] = args.top_k
        model.params['embedding_input_dim'] = preprocessor.context['vocab_size'] + 1
        model.params['embedding_output_dim'] = 300
        model.params['mlp_num_layers'] = args.mlp_num_layers
        model.params['mlp_num_units'] = 5
        model.params['mlp_num_fan_out'] = 1
        model.params['mlp_activation_func'] = 'tanh'
        model.params['optimizer'] = 'adagrad'
        model.guess_and_fill_missing_params()
        model.build()
        model.compile()
        model.backend.summary()

        model.load_embedding_matrix(embedding_matrix)
        
        train_path = os.path.join(args.dataset_path, f'train_{index}.csv')
        valid_path = os.path.join(args.dataset_path, f'valid_{index}.csv')
        test_path = os.path.join(args.dataset_path, f'test_{index}.csv')

        train_data_pack = load_data(path=train_path, task='ranking')
        valid_data_pack = load_data(path=valid_path, task='ranking')
        test_data_pack = load_data(path=test_path, task='ranking')
        print(train_data_pack.frame().head())
        
        train_data_pack_processed = preprocessor.transform(train_data_pack)
        valid_data_pack_processed = preprocessor.transform(valid_data_pack)
        test_data_pack_processed = preprocessor.transform(test_data_pack)

        print(train_data_pack_processed.frame().head())

        train_generator = mz.data_generator.PairDataGenerator(
            data_pack=train_data_pack_processed, num_dup=args.num_dup,
            num_neg=args.num_neg, batch_size=args.batch_size)
        model.fit_generator(train_generator, epochs=args.epochs, workers=10)

        save_result(model, args, train_data_pack_processed, name='train', index=index)
        save_result(model, args, valid_data_pack_processed, name='valid', index=index)
        save_result(model, args, test_data_pack_processed, name='test', index=index)

        model.save(os.path.join(args.model_path, f"drmm_w2v_d{args.num_dup}n{args.num_neg}_top{args.top_k}_eps{args.epochs}_bs{args.batch_size}_{index}"))


if __name__ == "__main__":
    # Load config
    args = config.get_args()
    train(args)
