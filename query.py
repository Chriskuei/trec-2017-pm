# -*- coding: utf-8 -*-
import os
import collections
import pandas as pd
import matchzoo as mz
import xml.etree.ElementTree as ET

from configs import config
from elasticsearch import Elasticsearch

def save_result(args, model, data_pack_processed, tag='train', index=0):
    """save result to csv"""
    x, _ = data_pack_processed.unpack()
    y_pred = model.predict(x)
    y_pred = [i[0] for i in y_pred]

    df = pd.DataFrame(
        {'id_left': x['id_left'], 'id_right': x['id_right'], 'score': y_pred})
    df.sort_values(by=['id_left', 'score'], ascending=[
                   True, False], inplace=True)
    df['Q0'] = 'Q0'
    df['WAW'] = 'WAW'
    df['blank'] = '0'
    df['rank'] = df.groupby('id_left')['score'].rank(
        ascending=False).astype(int)

    pred = df[['id_left', 'Q0', 'id_right', 'rank', 'score', 'WAW']]
    pred.to_csv(
        f"./output/es_drmm_w2v_d{args.num_dup}n{args.num_neg}_top{args.top_k}_eps{args.epochs}_bs{args.batch_size}_{tag}_{index}_r{args.result_size}.txt", sep='\t', index=False, header=False, mode='a')


def spilt_topics(query_file):
    order = [[28, 29, 25, 22, 6, 7], [26, 11, 1, 18, 21, 4], [19, 24, 27, 30, 12, 23], [13, 14, 3, 16, 8, 9],
             [15, 20, 5, 10, 17, 2]]
    tree = ET.parse(query_file)
    root = tree.getroot()
    query = {}
    topics = root.findall('topic')
    try:
        for index, item in enumerate(topics):
            extracted_data = collections.OrderedDict()
            tnum = index + 1
            disease = item.find('disease').text
            gene = item.find('gene').text
            demographic = item.find('demographic').text
            age = demographic.split('-')[0]
            gender = demographic.split(' ')[1]
            other = item.find('other').text
            extracted_data['tnum'] = tnum
            extracted_data['disease'] = disease
            extracted_data['gene'] = gene
            extracted_data['age'] = int(demographic.split('-')[0])
            extracted_data['sex'] = demographic.split(' ')[1]
            extracted_data['other'] = other
            extracted_data['string'] = (
                u" ".join(["disease", disease, "gene", gene, "age", age, "gender", gender]))
            query[str(tnum)] = extracted_data
    except:
        extracted_data['tnum'] = None
        extracted_data['disease'] = None
        extracted_data['gene'] = None
        extracted_data['age'] = None
        extracted_data['sex'] = None
        extracted_data['other'] = None
        extracted_data['string'] = None

    train_query = []
    validate_query = []
    test_query = []
    for i in range(5):
        train = [[0, 1, 2], [0, 1, 4], [0, 3, 4], [2, 3, 4], [1, 2, 3]][i]
        valid = [3, 2, 1, 0, 4][i]
        test = [4, 3, 2, 1, 0][i]
        train_query.append([query[str(idx)] for idx in order[train[0]] + order[train[1]] + order[train[2]]])
        validate_query.append([query[str(idx)] for idx in order[valid]])
        test_query.append([query[str(idx)] for idx in order[test]])


    return train_query, validate_query, test_query

def query(args):
    """
    The query topics are provided in an XML file. This function 
    is used to extract query terms from that XML file.
    
    After extracting the query terms, it is stored in an ordered
    dictionary. This dictionary is then passed to the es_query 
    function which will query Elasticsearch with those terms.
    """

    # Provide the path to the query xml file
    query_file = args.topic_path

    train_querys, validate_querys, test_querys = spilt_topics(query_file)

    # Load preprocessor
    preprocessor = mz.engine.base_preprocessor.load_preprocessor(args.prepro_model)

    for index in range(args.cv_num):

        train_query = train_querys[index]
        validate_query = validate_querys[index]
        test_query = test_querys[index]
        # Load drmm model
        model = mz.engine.base_model.load_model(os.path.join(args.model_path, f"drmm_w2v_d{args.num_dup}n{args.num_neg}_top{args.top_k}_eps{args.epochs}_bs{args.batch_size}_{index}"))
        for q in train_query:
            es_query(args, q, model, preprocessor, tag='train', index=index)
        for q in validate_query:
            es_query(args, q, model, preprocessor, tag='valid', index=index)
        for q in test_query:
            es_query(args, q, model, preprocessor, tag='test', index=index)


def es_query(args, extracted_data, model, preprocessor, tag='train', index=0):
    """
    This function is used to query Elasticsearch and write results
    to an output file.
    It receives a dictionary containing the extracted query terms
    from the query function. After querying Elasticsearch,
    the retrieved results are written to an output file in the
    standard trec_eval format.
    """
    try:
        # Store the disease name from the received dictionary in
        # the variable named query
        disease = extracted_data['disease']
        gene = extracted_data['gene']
        # best_field + [tie_breaker * _score for all other matching fields]
        res = es.search(index='ct', body={
            "query": {
                "bool": {
                    "must": [{
                        "multi_match": {
                            "query": disease,
                            "fields": ["brief_title^3", "brief_summary",
                                    "detailed_description", "condition",
                                    "eligibility", "keyword", "mesh_term",
                                    "official_title^2"],
                            "tie_breaker": 0.3,
                            "boost": 1.8
                        }}, {
                        "multi_match": {
                            "query": gene,
                            "fields": ["brief_title^3", "brief_summary",
                                    "detailed_description", "condition",
                                    "eligibility", "keyword", "mesh_term",
                                    "official_title^2"],
                            "tie_breaker": 0.3
                        }}
                    ]
                }
            },
            "post_filter": {
                "term": {
                    "gender": "all"
                }
            }
        }, size=args.result_size)['hits']['hits']
        #max_score = res[0]['_score']
        rank_ctr = 1
        # Write the retrieved results to an output file in the standard trec_eval format
        with open(f'./output/es_{tag}_{index}_r{args.result_size}.txt', 'a') as op_file:
            for i in res:
                op_file.write('{}\tQ0\t{}\t{}\t{}\tWAW\n'.format(
                    extracted_data['tnum'], i['_source']['nct_id'], rank_ctr, i['_score']))
                rank_ctr += 1
        
        re_ranking(args, res, extracted_data, model, preprocessor, tag=tag, index=index)
    except Exception as e:
        print("\nUnable to query/write!")
        print('Error Message:', e, '\n')
        


def re_ranking(args, res, extracted_data, model, preprocessor, tag='train', index=0):
    """
    Re ranking the document using DRMM_TKS model
    """
    document = {}
    for item in res:
        try:
            doc_id = item['_source']['nct_id']
        except:
            doc_id = ""
        try:
            brief_title = item['_source']['brief_title']
        except:
            brief_title = ""
        try:
            official_title = item['_source']['official_title']
        except:
            official_title = ""
        try:
            brief_summary = item['_source']['brief_summary']
        except:
            brief_summary = ""
        try:
            detailed_description = item['_source']['detailed_description']
        except:
            detailed_description = ""
        try:
            overall_status = item['_source']['overall_status']
        except:
            overall_status = ""
        try:
            condition = item['_source']['condition']
        except:
            condition = ""
        try:
            eligibility = item['_source']['eligibility']
        except:
            eligibility = ""
        try:
            gender = item['_source']['gender']
        except:
            gender = ""
        try:
            gender_based = item['_source']['gender_based']
        except:
            gender_based = ""
        try:
            minimum_age = item['_source']['minimum_age']
        except:
            minimum_age = ""
        try:
            maximum_age = item['_source']['maximum_age']
        except:
            maximum_age = ""
        try:
            keyword = item['_source']['keyword']
        except:
            keyword = ""
        try:
            mesh_term = item['_source']['mesh_term']
        except:
            mesh_term = ""
        
        document_list = ["brief_title", brief_title, "official_title", official_title, "brief_summary",
                        brief_summary, "detailed_description", detailed_description, "overall_status",
                        overall_status, "condition", condition, "eligibility", eligibility, "gender",
                        gender, "gender_based", gender_based, "minimum_age", minimum_age, "maximum_age",
                        maximum_age, "keyword", keyword, "mesh_term", mesh_term]
        document_list = ["" if i is None else str(i) for i in document_list]
        document[str(doc_id)] = (u" ".join(document_list)).strip()

    df = pd.DataFrame(list(document.items()), columns=['id_right', 'text_right'])
    df['id_left'] = extracted_data['tnum']
    df['text_left'] = extracted_data['string']
    
    data_pack = mz.pack(df)
    # print(data_pack.frame().head())
    data_pack_processed = preprocessor.transform(data_pack)
    # print(data_pack_processed.frame().head())
    save_result(args, model, data_pack_processed, tag=tag, index=index)
    


if __name__ == '__main__':
    # Read configs
    args = config.get_args()

    # Create connection to Elasticsearch listening on localhost port 9200. 
    # It uses the Python Elasticsearch API which is the official low-level
    # client for Elasticsearch.
    try:
        es = Elasticsearch(
            [{'host': 'localhost', 'port': 9200}])
    except Exception as e:
        print('\nCannot connect to Elasticsearch!')
        print('Error Message:', e, '\n')
    # Call the function to start extracting the queries

    query(args)
