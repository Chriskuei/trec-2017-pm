# -*- coding: utf-8 -*-

import numpy as np
from gensim.models import word2vec
import glob
import os
import codecs

def extract_data_txt(list_of_files):
    docs = []
    for input_file in list_of_files:
        doc = []
        with open(input_file, "r") as f:
            for l in f:
                word = l.strip().split(" ")
                doc.extend(word)
                # print(doc)
            docs.append(doc)
    return docs


if __name__ == '__main__':

    data_path = './dataset/'

    # if not os.path.exists(data_path):
    #     os.makedirs(data_path)

    # list_of_files = glob.glob('./data/clinicaltrials_txt/*/*' + '/*.txt')
    # documents = extract_data_txt(list_of_files)

    # model = word2vec.Word2Vec(sentences=documents, min_count=1, size=300, workers=8)
    # model.save(data_path + 'w2v.model')

    word2vec_model = word2vec.Word2Vec.load(data_path + 'w2v.model')
    # vocab = ['我', '是', '一个', '学生', ...]
    vocab = word2vec_model.wv.vocab
    # vocab_idx={'我':0, '是':1, ...}
    vocab_idx = {}
    # word_vector=[[0.5,...,0.9],...]



    i = 0
    with codecs.open(data_path + "word_embedding_300d.txt", "w", encoding="utf-8") as f:
        for word in vocab:
            f.write(word + " ")
            f.write(" ".join(str(num) for num in word2vec_model[word]))
            f.write("\n")
            vocab_idx[word] = str(i)
            i += 1

    with codecs.open(data_path + "word2vec_vocab.txt", "w", encoding="utf-8") as f:
        for word in vocab_idx:
            f.write("%s\t%s\n" % (vocab_idx[word], word))