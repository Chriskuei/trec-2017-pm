# -*- coding: utf-8 -*-
import os
import csv
import time
import glob
import collections
import pandas as pd
import numpy as np
import xml.etree.ElementTree as ET


train = []


def extract_query_xml(query_file):
    tree = ET.parse(query_file)
    root = tree.getroot()
    query = {}
    topics = root.findall('topic')
    for index, item in enumerate(topics):
        tnum = index + 1
        disease = item.find('disease').text
        gene = item.find('gene').text
        demographic = item.find('demographic').text

        age = demographic.split('-')[0]
        gender = demographic.split(' ')[1]

        query[str(tnum)] = (
            u" ".join(["disease", disease, "gene", gene, "age", age, "gender", gender]))

    return query


def extract_data_xml(list_of_files):

    ctr = 0
    # We will print the progress as we process each file
    print('\nProgress:')
    document = {}
    for input_file in list_of_files:
        tree = ET.parse(input_file)
        root = tree.getroot()

        # Create an ordered dictionary and lists to store the keywords and mesh terms
        # extracted_data = collections.OrderedDict()
        keyword_list = []
        mesh_term_list = []
        # train_item = ()

        # nct_id
        try:
            nct_id = root.find('id_info').find('nct_id').text
            doc_id = nct_id
        except:
            doc_id = "None"

        # brief_title
        try:
            brief_title = root.find('brief_title').text
            # extracted_data['brief_title'] = brief_title
        except:
            brief_title = "None"

        # official_title
        try:
            official_title = root.find('official_title').text
            # extracted_data['brief_title'] = brief_title
        except:
            official_title = "None"

        # brief_summary
        try:
            brief_summary = root.find('brief_summary').find('textblock').text
        #     extracted_data['brief_summary'] = brief_summary
        except:
            brief_summary = "None"

        # detailed_description
        try:
            detailed_description = root.find(
                'detailed_description').find('textblock').text
            # extracted_data['detailed_description'] = detailed_description
        except:
            detailed_description = "None"

        # overall_status
        try:
            overall_status = root.find('overall_status').text
            # extracted_data['overall_status'] = overall_status
        except:
            overall_status = "None"

        # condition
        try:
            condition = root.find('condition').text
            # extracted_data['condition'] = condition
        except:
            condition = "None"

        # eligibility
        try:
            eligibility = root.find('eligibility').find(
                'criteria').find('textblock').text
            # extracted_data['eligibility'] = eligibility
        except:
            eligibility = "None"

        # gender
        try:
            gender = root.find('eligibility').find('gender').text
            # extracted_data['gender'] = gender
        except:
            gender = "None"

        # gender_based
        try:
            gender_based = root.find('eligibility').find('gender_based').text
            # extracted_data['gender_based'] = gender_based
        except:
            gender_based = "None"

        # minimum_age
        try:
            minimum_age = root.find('eligibility').find('minimum_age').text
            try:
                minimum_age = minimum_age.split(' ')[0]
            except:
                minimum_age = 0
        except:
            minimum_age = "None"

        # maximum_age
        try:
            maximum_age = root.find('eligibility').find('maximum_age').text
            try:
                maximum_age = maximum_age.split(' ')[0]
            except:
                maximum_age = 99
        except:
            maximum_age = "None"

        # keyword
        try:
            keyword = root.findall('keyword')
            for _, item in enumerate(keyword):
                keyword_list.append(item.text)
            keyword = u" ".join(keyword_list)
        except:
            keyword = "None"

        # mesh_term
        try:
            mesh_term = root.find('condition_browse').findall('mesh_term')
            for _, item in enumerate(mesh_term):
                mesh_term_list.append(item.text)
            mesh_term = u" ".join(mesh_term_list)
        except:
            mesh_term = "None"

        document[str(doc_id)] = (u" ".join(["brief_title", brief_title, "official_title", official_title, "brief_summary",
                                            brief_summary, "detailed_description", detailed_description, "overall_status",
                                            overall_status, "condition", condition, "eligibility", eligibility, "gender",
                                            gender, "gender_based", gender_based, "minimum_age", minimum_age, "maximum_age",
                                            maximum_age, "keyword", keyword, "mesh_term", mesh_term])).encode('utf-8').strip()
        # Increment the counter and print the progress in the following format: current counter value/total number of input files.
        ctr += 1
        # print(ctr, '/', len(list_of_files))
    return document


# Note the start time
start_time = time.time()


if __name__ == '__main__':
    train_full = []

    if not os.path.exists("dataset"):  # 如果路径不存在
        os.makedirs("dataset")
    qids = []
    docids = []
    levels = []
    qds = []

    query_file = "./data/topics2017.xml"
    queries = extract_query_xml(query_file)
    # Provide the path to the input xml files
    list_of_files = glob.glob('./data/clinicaltrials_xml/*/*' + '/*.xml')
    documents = extract_data_xml(list_of_files)

    with open("./data/qrels-final-trials.txt", "r") as f:
        for l in f:
            qid, other, docid, level = l.strip().split(" ")
            qids.append(int(qid))
            train_full.append(
                [qid, docid, queries[qid], documents[docid], level])

    lenth = len(train_full)
    order = [[28, 29, 25, 22, 6, 7], [26, 11, 1, 18, 21, 4], [19, 24, 27, 30, 12, 23], [13, 14, 3, 16, 8, 9],
             [15, 20, 5, 10, 17, 2]]

    for i in range(5):
        with open('dataset/train_%d.csv' % i, 'w') as f, open('dataset/valid_%d.csv' % i, 'w') as w1, open('dataset/test_%d.csv' % i,
                                                                                                           'w') as w2:

            writer_f = csv.writer(f, delimiter="\t")
            writer_w1 = csv.writer(w1, delimiter="\t")
            writer_w2 = csv.writer(w2, delimiter="\t")
            writer_f.writerow(["qid", "docid", "q_text", "d_text", "label"])
            writer_w1.writerow(["qid", "docid", "q_text", "d_text", "label"])
            writer_w2.writerow(["qid", "docid", "q_text", "d_text", "label"])
            train = [[0, 1, 2], [0, 1, 4], [0, 3, 4], [2, 3, 4], [1, 2, 3]][i]
            valid = [3, 2, 1, 0, 4][i]
            test = [4, 3, 2, 1, 0][i]
            train_qd = [train_full[idx] for idx in range(lenth) if
                        qids[idx] in order[train[0]] + order[train[1]] + order[train[2]]]
            validate_qd = [train_full[idx]
                           for idx in range(lenth) if qids[idx] in order[valid]]
            test_qd = [train_full[idx]
                       for idx in range(lenth) if qids[idx] in order[test]]

            for item in train_qd:
                writer_f.writerow(item)
            for item in validate_qd:
                writer_w1.writerow(item)
            for item in test_qd:
                writer_w2.writerow(item)

print("\nExecution time: %.2f seconds" % (time.time()-start_time))
