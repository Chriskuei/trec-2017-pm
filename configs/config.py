import argparse
import os

def get_parser():
    data_path = './data'
    model_path = './model'
    output_path = './output'
    dataset_path = './dataset'

    parser = argparse.ArgumentParser()
    parser.add_argument('--data_path', default=data_path)
    parser.add_argument('--model_path', default=model_path)
    parser.add_argument('--output_path', default=output_path)
    parser.add_argument('--dataset_path', default=dataset_path)
    parser.add_argument('--topic_path', default=os.path.join(data_path, 'topics2017.xml'))
    parser.add_argument('--qrel_path', default=os.path.join(data_path, 'qrels-final-trials.txt'))
    parser.add_argument('--ct_txt', default=os.path.join(data_path, 'clinicaltrials_txt'))
    parser.add_argument('--ct_xml', default=os.path.join(data_path, 'clinicaltrials_xml'))
    parser.add_argument('--prepro_model', default=os.path.join(model_path, 'preprocessor'))
    parser.add_argument('--drmm_model', default=os.path.join(model_path, 'drmm'))
    parser.add_argument('--embed_path', default=os.path.join(dataset_path, 'word2vec.ct.300d.txt'))
    # parser.add_argument('--embed_path', default=os.path.join(dataset_path, 'glove.6B.300d.txt'))
    parser.add_argument('--epochs', default=50, type=int)
    parser.add_argument('--top_k', default=20, type=int)
    parser.add_argument('--cv_num', default=5, type=int)
    parser.add_argument('--mlp_num_layers', default=5, type=int)
    parser.add_argument('--batch_size', default=32, type=int)
    parser.add_argument('--num_dup', default=5, type=int)
    parser.add_argument('--num_neg', default=1, type=int)
    parser.add_argument('--result_size', default=20, type=int)
    parser.add_argument('--verbose', default=True, type = bool)
    return parser

def get_args():
    parser = get_parser()
    args = parser.parse_args()
    return args